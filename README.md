# C++ template for tdx-wayland SDK #

This is template project for Eclipse Neon3

### What is this repository for? ###

* Use this template to making C++ software for Toradex remote device 


### How do I get set up? ###

* Follow these instructions
* !Dont forget to setup enviroment variables!
* 1)[Configure Eclipse](https://developer.toradex.com/getting-started/module-2-my-first-hello-world-in-c/configure-eclipse)
* 2)[Debug using Eclipse](https://developer.toradex.com/getting-started/module-2-my-first-hello-world-in-c/configure-eclipse)
* 3)For Cross G++ Compiler set comand : ${CXX}
* 4)For Cross G++ Compiler Miscelanous set flags : ${CXXFLAGS} -c
* 5)Be careful to set proper name of file and path in Remote Absolut File Path for C/C++ Application, this can result in No Such File error
 
### Should be ready to Go ###

### Who do I talk to? ###

* Repo owner 
